import https from 'https';
import fs from 'fs';
import path from 'path';

function download(url, targetPath, targetName) {
  const target = path.join(targetPath, url.match(/^.+\.exe$/) ? `vendor/${targetName}.exe` : `vendor/${targetName}`);
  const stream = fs.createWriteStream(target);
  console.log(`[Bitdropr] Downloading ${url} to ${target}.`);
  https.get(url, response => response.pipe(stream));
}

function add(dep, macarch, winarch, module) {
  const targetPath = path.dirname(require.resolve(module || dep));

  // eslint-disable-next-line import/no-dynamic-require
  const version = require(path.join(targetPath, 'package.json')).version;

  const darwinUrl = `https://raw.githubusercontent.com/imagemin/${dep}-bin/v${version}/vendor/macos/${macarch ? `${macarch}/` : ''}${dep}`;
  const winUrl = `https://raw.githubusercontent.com/imagemin/${dep}-bin/v${version}/vendor/win/${winarch ? `${winarch}/` : ''}${dep}.exe`;

  console.log(`[Bitdropr] Downloading additional ${dep} executables...`);

  switch (process.platform) {
    case 'darwin':
      download(winUrl, targetPath, dep);
      break;
    case 'win32':
      download(darwinUrl, targetPath, dep);
      break;
    default:
      download(darwinUrl, targetPath, dep);
      download(winUrl, targetPath, dep);
      break;
  }

  console.log('[Bitdropr] Done.');
}

add('guetzli');
add('gifsicle', undefined, 'x64');
add('pngquant', undefined, undefined, 'pngquant-bin');
