import { app, BrowserWindow, ipcMain } from 'electron'; // eslint-disable-line
import fs from 'fs';
import path from 'path';
import { Buffer }from 'buffer'; // eslint-disable-line

require('hazardous');

const imagemin = require('imagemin');
const imageminGuetzli = require('imagemin-guetzli');
const imageminPngquant = require('imagemin-pngquant');
const imageminSvgo = require('imagemin-svgo');
const imageminGifsicle = require('imagemin-gifsicle');


/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\'); // eslint-disable-line
}

let mainWindow;
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080'
  : `file://${__dirname}/index.html`;

const imageminConf = {
  jpeg: {
    plugins: [
      imageminGuetzli({
        quality: 95, // 1 - 100
        memlimit: 6000,
        nomemlimit: false,
      }),
    ],
  },
  png: {
    plugins: [
      imageminPngquant({
        floyd: 0.5,
        nofs: false,
        posterize: 0,
        quality: 95, // 1 - 100
        speed: 3,
        verbose: false,
        strip: process.platform === 'darwin',
      }),
    ],
  },
  svg: {
    plugins: [
      imageminSvgo({
        plugins: {
          minifyStyles: true,
        },
      }),
    ],
  },
  gif: {
    plugins: [
      imageminGifsicle({
        interlaced: false,
        optimizationLevel: 3, // 1 - 3
        colors: 256, // 2 - 256
      }),
    ],
  },
};

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 800,
    width: 1400,
    useContentSize: true,
    icon: require('path').join(__dirname, '/build/icons/64x64.png'),
  });

  if (process.env.NODE_ENV === 'development') {
    mainWindow.webContents.openDevTools();
  }

  ipcMain.on('loadFile', (event, args) => {
    const { filePath, buildPath, fileName, debugInfo, fileType } = args;

    if (!fileType.startsWith('image/')) {
      return;
    }

    const outputPath = path.resolve(buildPath, fileName);
    const readStream = fs.createReadStream(filePath);
    const dataArray = [];
    const ext = fileType.substr('image/'.length);

    if (!fs.existsSync(buildPath)) {
      fs.mkdirSync(buildPath);
    }

    const log = require('electron-log');
    log.transports.file.level = 'info';
    log.transports.file.file = path.join(buildPath, 'log.log');
    log.info(debugInfo);
    // eslint-disable-next-line import/no-extraneous-dependencies

    readStream
      .on('data', data => dataArray.push(...data))
      .on('end', () => {
        log.info(`Compressing ${filePath} to ${outputPath}...`);

        const result = imagemin.buffer(Buffer.from(dataArray), imageminConf[ext] ?
          imageminConf[ext] : imageminConf.jpeg);

        result
          .then((buffer) => {
            log.info(`buildPath: ${buildPath}`);

            fs.writeFile(outputPath, buffer);
          })
          .catch(err => log.error(err));
      });
  });

  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
